package com.paic.arch.interviews.constant;

public class CommonConstant {

	// 4个红灯
	public static final String RED_LAMP_ON_FOUR = "RRRR";
	// 4个蓝灯
	public static final String YELLOW_LAMP_ON_FOUR = "YYYY";
	// 11个亮灯
	public static final String LAMP_ON_ELEVEN = "YYRYYRYYRYY";

	public static final String NEWLINE = System.getProperty("line.separator");

}
