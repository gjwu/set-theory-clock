package com.paic.arch.interviews.util;

import com.paic.arch.interviews.constant.LampColorEnum;

public class LampCalculateUtil {

	/**
	 * 根据亮灯数,获取实际蓝灯字符串
	 * @param lampOnStr		该排所有灯都亮
	 * @param lampOnCount	实际亮灯数
	 * @return
	 */
	public static String calculate(String lampOnStr, int lampOnCount) {
		if (lampOnStr == null) {
			throw new RuntimeException("parameter[lampOnStr] must not be null");
		}

		if (lampOnCount > lampOnStr.length()) {
			throw new RuntimeException("parameter[lampOnCount] must be less than parameter[lampOnStr] length");
		}

		StringBuffer rs = new StringBuffer();
		if (lampOnCount < 0) {
			lampOnCount = 0;

		} else if (lampOnCount > 0) {
			rs.append(lampOnStr.substring(0, lampOnCount));
		}

		for (int i = lampOnCount; i < lampOnStr.length(); i++) {
			rs.append(LampColorEnum.O.name());
		}
		return rs.toString();
	}

}
