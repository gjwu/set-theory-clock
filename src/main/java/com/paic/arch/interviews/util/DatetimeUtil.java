package com.paic.arch.interviews.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatetimeUtil {

	public static String DEFAULT_DATE_HMS = "HH:mm:ss";

	public static Date convertStrToDate(String date, String format) throws ParseException {
		if (null == date || "".equals(date))
			return null;
		return new SimpleDateFormat(format).parse(date);
	}

	public static int getHour(String date) {
		return Integer.parseInt(date.split(":")[0]);
	}

	public static int getMinute(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	public static int getSecond(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.SECOND);
	}
}
