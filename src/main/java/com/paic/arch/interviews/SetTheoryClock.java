package com.paic.arch.interviews;

import java.text.ParseException;
import java.util.Date;

import com.paic.arch.interviews.constant.CommonConstant;
import com.paic.arch.interviews.constant.LampColorEnum;
import com.paic.arch.interviews.util.DatetimeUtil;
import com.paic.arch.interviews.util.LampCalculateUtil;

public class SetTheoryClock implements TimeConverter {

	@Override
	public String convertTime(String aTime) {
		StringBuffer result = new StringBuffer();
		Date date = null;

		try {
			date = DatetimeUtil.convertStrToDate(aTime, DatetimeUtil.DEFAULT_DATE_HMS);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(String.format("time[%s] convert exectpion", aTime));
		}

		int hour = DatetimeUtil.getHour(aTime);
		int minute = DatetimeUtil.getMinute(date);
		int second = DatetimeUtil.getSecond(date);
		// System.out.println(hour+"-"+minute+"-"+second);

		// 处理第一排:每两秒钟闪烁一次的黄灯
		result.append(second % 2 == 0 ? LampColorEnum.Y.name() : LampColorEnum.O.name());
		result.append(CommonConstant.NEWLINE);

		// 处理第二排小时 :有4盏灯，每盏红色灯代表5小时
		int showLampIndex = hour / 5;
		result.append(LampCalculateUtil.calculate(CommonConstant.RED_LAMP_ON_FOUR, showLampIndex));
		result.append(CommonConstant.NEWLINE);

		// 处理第三排小时:有4盏灯，每盏红色灯代表1小时
		showLampIndex = hour % 5;
		result.append(LampCalculateUtil.calculate(CommonConstant.RED_LAMP_ON_FOUR, showLampIndex));
		result.append(CommonConstant.NEWLINE);

		// 处理第四排有11盏灯，每盏灯代表5分钟，其中第3、第6和第9盏的灯是红色，表示15分、30分和45分，其他的灯为黄色
		showLampIndex = minute / 5;
		result.append(LampCalculateUtil.calculate(CommonConstant.LAMP_ON_ELEVEN, showLampIndex));
		result.append(CommonConstant.NEWLINE);

		// 处理第五排:有4盏灯，每盏蓝色灯代表1分钟
		showLampIndex = minute % 5;
		result.append(LampCalculateUtil.calculate(CommonConstant.YELLOW_LAMP_ON_FOUR, showLampIndex));

		return result.toString();
	}

	
}
